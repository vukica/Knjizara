DROP SCHEMA IF EXISTS knjizara;
CREATE SCHEMA knjizara DEFAULT CHARACTER SET utf8;
USE knjizara;

CREATE TABLE knjige (
	knjiga_id int auto_increment,
    naziv VARCHAR(30) NOT NULL,
    cena decimal(6,2) not null,
    PRIMARY KEY (knjiga_id)
);

CREATE TABLE adresa(
	adresa_id INT AUTO_INCREMENT,
    ulica VARCHAR(20) NOT NULL, 
    broj int NOT NULL, 
    grad VARCHAR(10), 
    drzava varchar(10) not null,
    PRIMARY KEY (adresa_id)
);
create table korisnik(
korisnik_id int auto_increment,
ime varchar(20) not null,
prezime varchar(20) not null,
email varchar(20) not null,
adresa int not null,
primary key(korisnik_id),
foreign key(adresa) references adresa(adresa_id)
);
create table jeKupio(
korisnik int,
knjiga int,
kolicina int,
foreign key(knjiga) references knjige(knjiga_id),
foreign key(korisnik) references korisnik(korisnik_id)
);
INSERT INTO knjige(naziv, cena) VALUES ('Introduction to Algorithms', 750.00);
INSERT INTO knjige(naziv, cena) VALUES ('Introduction to Probability', 949.99);
INSERT INTO knjige(naziv, cena) VALUES ('Digital Images Processing', 1340.50);
INSERT INTO knjige(naziv, cena) VALUES ('Image Segmentation', 385.60);
INSERT INTO knjige(naziv, cena) VALUES ('Linear Algebra', 2335.30);
INSERT INTO knjige(naziv, cena) VALUES ('Numerical Linear Algebra', 385.60);

insert into adresa(ulica,broj,grad,drzava) values ('Santiceva',99,'Novi Sad','Srbija');
insert into adresa(ulica,broj,grad,drzava) values ('Dunavska',33,'Novi Sad','Srbija');
insert into adresa(ulica,broj,grad,drzava) values ('Bahnstrasse',222,'Bern','Svajcarska');
insert into adresa(ulica,broj,grad,drzava) values ('Kociceva',21,null,'Srbija');

INSERT INTO korisnik (ime, prezime,email, adresa) VALUES ('Ana', 'Simic', 'ana@gmail.com',2);
INSERT INTO korisnik (ime, prezime,email, adresa) VALUES ('Petar', 'Peric', 'petar@gmail.com',1);
INSERT INTO korisnik (ime, prezime,email, adresa) VALUES ('Milan', 'Peric', 'milan@gmail.com',3);

insert into jeKupio(korisnik,knjiga,kolicina) values (3,1,1);
insert into jeKupio(korisnik,knjiga,kolicina) values (2,1,1);
insert into jeKupio(korisnik,knjiga,kolicina) values (2,2,3);
insert into jeKupio(korisnik,knjiga,kolicina) values (2,3,3);
