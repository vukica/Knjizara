package model;

public class JeKupio {
private Korisnik korisnik;
private Knjiga proizvod;
private int kolicina;
public JeKupio(Korisnik korisnik, Knjiga proizvod, int kolicina) {
	super();
	this.korisnik = korisnik;
	this.proizvod = proizvod;
	this.kolicina = kolicina;
}
public Korisnik getKorisnik() {
	return korisnik;
}
public void setKorisnik(Korisnik korisnik) {
	this.korisnik = korisnik;
}
public Knjiga getProizvod() {
	return proizvod;
}
public void setProizvod(Knjiga proizvod) {
	this.proizvod = proizvod;
}
public int getKolicina() {
	return kolicina;
}
public void setKolicina(int kolicina) {
	this.kolicina = kolicina;
}
@Override
public String toString() {
	return "JeKupio [korisnik=" + korisnik + ", proizvod=" + proizvod + ", kolicina=" + kolicina + "]";
}

}
