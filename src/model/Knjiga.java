package model;

public class Knjiga {
private int id;	
private String naziv;
private double cena;
public Knjiga() {
	super();
}
public Knjiga(String naziv, double cena) {
	super();
	this.naziv = naziv;
	this.cena = cena;
}

public Knjiga(int id, String naziv, double cena) {
	super();
	this.id = id;
	this.naziv = naziv;
	this.cena = cena;
}
public String getNaziv() {
	return naziv;
}
public void setNaziv(String naziv) {
	this.naziv = naziv;
}
public double getCena() {
	return cena;
}
public void setCena(double cena) {
	this.cena = cena;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
@Override
public String toString() {
	return String.format("%15s %6.2f", naziv,cena);
}

}
