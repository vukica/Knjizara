package model;

public class Korisnik {
private int id;	
private String ime;
private String prezime;
private String email;
private Adresa adresa;
public Korisnik() {
	super();
}
public Korisnik(String ime, String prezime, String email, Adresa adresa) {
	super();
	this.ime = ime;
	this.prezime = prezime;
	this.email = email;
	this.adresa = adresa;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getIme() {
	return ime;
}
public void setIme(String ime) {
	this.ime = ime;
}
public String getPrezime() {
	return prezime;
}
public void setPrezime(String prezime) {
	this.prezime = prezime;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public Adresa getAdresa() {
	return adresa;
}
public void setAdresa(Adresa adresa) {
	this.adresa = adresa;
}
@Override
public String toString() {
	return String.format("%10s %10s %15s", ime,prezime,email);
}

}
