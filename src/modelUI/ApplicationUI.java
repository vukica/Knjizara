package modelUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ApplicationUI {
private static Connection conn;
	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/knjizara?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			System.exit(0);
		}
		
	}
	public static void main(String[] args) {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = utils.PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				KnjigaUI.menu();
				break;
			case 2:
				KorisnikUI.menu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}

		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void ispisiMenu() {
		System.out.println("Kupovina - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa knjigama");
		System.out.println("\tOpcija broj 2 - Rad sa korisnicima");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}
}


