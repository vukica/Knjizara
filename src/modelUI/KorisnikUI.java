package modelUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Korisnik;
import modelDAO.KorisnikDAO;

public class KorisnikUI {
	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = utils.PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveKorisnikeSaAdresama();
				break;
			case 2:
				prikaziKorisnikaSaID();
				break;
			case 3:
				kupciIzSvajcarske();
				break;
			case 4:
				sortiraniPoBrojuKupljenihKnjiga();
				break;
			case 5:
				korisniciCijiJeGradNull();
				break;
			case 6:
				prikazOnihKojiNisuKupovali();
				break;
			case 7:
				prikazDrzavaPoUkupnoKorisnika();
				break;
			case 8:
				prikazDrzavaPoUkupnojZaradi();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa korisnicima - opcije:");
		System.out.println("\tOpcija broj 1 - Ispis svih korisnika sa adresama");
		System.out.println("\tOpcija broj 2 - Prikaz korisnika sa zadatim id");
		System.out.println("\tOpcija broj 3 - Korisnici iz Svajcarske koji su kupovali");
		System.out.println("\tOpcija broj 4 - Sortiranje po broju ukupno kupljenih knjiga");
		System.out.println("\tOpcija broj 5 - Svi korisnici kojima je grad null");
		System.out.println("\tOpcija broj 6 - Svi korisnici koji nisu kupili nijednu knjigu");
		System.out.println("\tOpcija broj 7 - Prikaz drzava po ukupnom broju korisnika");
		System.out.println("\tOpcija broj 8 - Prikaz drzava po ukupnoj zaradi");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}
	public static void ispisiSveKorisnikeSaAdresama() {
		List<Korisnik> korisnici = KorisnikDAO.getAllSaProsirenimAdresama(ApplicationUI.getConn());
		System.out.printf("%10s %10s %15s %10s %5s %10s %10s \n",
				"ime","prezime","email","ulica","broj","grad","drzava");
		System.out.println("----------------------------------------------------------------------------");
		for(int i = 0; i < korisnici.size(); i++) {
			Korisnik korisnik = korisnici.get(i);
			if(korisnik.getAdresa().getGrad() != null) {
				System.out.println(korisnik + " " + korisnik.getAdresa());
			}else {
				System.out.printf("%10s %10s %15s %10s %5d %10s %10s \n",
						korisnik.getIme(),korisnik.getPrezime(),korisnik.getEmail(),
						korisnik.getAdresa().getUlica(),korisnik.getAdresa().getBroj(),
						" ", korisnik.getAdresa().getDrzava());
			}
		}
	}
	public static Korisnik pronadjiKorisnikaSaId(int id) {
		Korisnik korisnik = KorisnikDAO.getKorisnikById(ApplicationUI.getConn(), id);
		return korisnik;
		
	}
	public static void prikaziKorisnikaSaID() {
		System.out.println("Unesite id korisnika");
		int id = utils.PomocnaKlasa.ocitajCeoBroj();
		Korisnik korisnik = pronadjiKorisnikaSaId(id);
		System.out.println(korisnik);
	}
	
	public static void kupciIzSvajcarske() {
		//prikazacu i adresu
		ArrayList<Korisnik> korisnici = KorisnikDAO.sviKupciIzSvajcarske(ApplicationUI.getConn());
		if(korisnici.size() == 0) {
			System.out.println("Lista kupaca iz Svajcarske je prazna.");
			return;
		}
		System.out.println("Korisnici koji su obavili kupovinu iz Svajcarske su");
		for(int i = 0; i < korisnici.size(); i++) {
			System.out.println(korisnici.get(i) + " "+ korisnici.get(i).getAdresa());
		}
	}
	public static void sortiraniPoBrojuKupljenihKnjiga() {
		HashMap<Korisnik, Integer > rezultat = KorisnikDAO.sortiraniPoBrojuKupljenihKnjiga(ApplicationUI.getConn());
		for(Korisnik key : rezultat.keySet()) {
			System.out.println(key + " " + rezultat.get(key));
		}
	}
	public static void prikazOnihKojiNisuKupovali() {
		HashMap<Korisnik, Integer > rezultat = KorisnikDAO.sortiraniPoBrojuKupljenihKnjiga(ApplicationUI.getConn());
		for(Korisnik key : rezultat.keySet()) {
			if(rezultat.get(key) == 0)
			System.out.println(key + " " + rezultat.get(key));
		}
	}
	public static void korisniciCijiJeGradNull() {
		ArrayList<Korisnik> korisnici = KorisnikDAO.sviKupciCijiJeGradNull(ApplicationUI.getConn());
		System.out.println("Korisnici ciji je grad null su:");
		if(korisnici.isEmpty()) {
			System.out.println("Ne postoje korisnici ciji je grad null");
		}else {
			for(int i = 0; i < korisnici.size(); i++) {
				System.out.println(korisnici.get(i) + " "+ korisnici.get(i).getAdresa());
			}
		}
	}
	public static void prikazDrzavaPoUkupnoKorisnika() {
		HashMap<String, Integer > rezultat = 
				KorisnikDAO.sortiraniDrzavaPoUkupnoKorisnika(ApplicationUI.getConn());
		System.out.printf("%10s %6s\n","Drzava","Ukupno");
		System.out.println("========== ======");
		for(String key : rezultat.keySet()) {
			System.out.printf("%10s %6d\n",key,rezultat.get(key));
		}

	}
	public static void prikazDrzavaPoUkupnojZaradi() {
		HashMap<String, Double > rezultat = 
				KorisnikDAO.sortiraniDrzavaPoUkupnojZaradi(ApplicationUI.getConn());
		System.out.printf("%10s %6s\n","Drzava","Ukupno");
		System.out.println("========== ======");
		for(String key : rezultat.keySet()) {
			System.out.printf("%10s %6.2f\n",key,rezultat.get(key));
		}
	}
}
