package modelUI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import io.bretty.console.table.Alignment;
import io.bretty.console.table.ColumnFormatter;
import io.bretty.console.table.Precision;
import io.bretty.console.table.Table;
import model.Korisnik;
import model.Knjiga;
import modelDAO.KnjigaDAO;

public class KnjigaUI {
	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = utils.PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				knjigeKupljeneUSrbiji();
				break;
			case 2:
				knjigeKojeNikoNijeKupio();
				break;
			case 3:
				ukupnoKupljenoPoDrzavama();
				break;
			case 4:
				try {
					knjigeCijiJeKupac();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa knjigama - opcije:");
		System.out.println("\tOpcija broj 1 - Ispis knjiga kupljenih u Srbiji");
		System.out.println("\tOpcija broj 2 - Ispis knjiga koje niko nije kupio");
		System.out.println("\tOpcija broj 3 - Ukupno knjiga kupljenih po drzavama");
		System.out.println("\tOpcija broj 4 - Knjige zadatog kupca");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}
	public static void knjigeKupljeneUSrbiji() {
		ArrayList<Knjiga> knjige = KnjigaDAO.knjigeKupljeneUSrbiji(ApplicationUI.getConn());
		if(knjige.isEmpty()) {
			System.out.println("Prazna lista knjiga kupljenih u Srbiji.");
		}else {
			System.out.println("Knjige kupljene u Srbiji su:");
			biblioteka(knjige);
		}
	}
	public static void knjigeKojeNikoNijeKupio() {
		ArrayList<Knjiga> knjige = KnjigaDAO.knjigeKojeNikoNijeKupio(ApplicationUI.getConn());
		if(knjige.size() == 0) {
			System.out.println("Prazna lista knjiga koje niko nije kupio.");
		}else {
			biblioteka(knjige);
		}
	}
	public static void ukupnoKupljenoPoDrzavama() {
		HashMap<String, Integer> rezultat = KnjigaDAO.sortiraniPoBrojuKupljenihKnjiga(ApplicationUI.getConn());
		System.out.printf("%10s %6s\n","Drzava","Ukupno");
		System.out.println("========== ======");
		for(String key : rezultat.keySet()) {
			System.out.printf("%10s %6d\n" ,key,rezultat.get(key));
		}
		String sP = System.getProperty("file.separator");
		File file = new File("."+sP+"data"+sP+"izvestaj.txt");
		try {
			PrintWriter out2 = new PrintWriter(new FileWriter(file),true);
			out2.println("Ukupno kupljeno knjiga po drzavama");
			for(String key : rezultat.keySet()) {
				out2.println(key + " " + rezultat.get(key));
			}
			out2.flush();
			out2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void knjigeCijiJeKupac() throws FileNotFoundException {
		System.out.println("Unesite id korisnika");
		int id = utils.PomocnaKlasa.ocitajCeoBroj();
		Korisnik korisnik = KorisnikUI.pronadjiKorisnikaSaId(id);
		ArrayList<Knjiga> knjige = KnjigaDAO.knjigeCijiJeKupac(ApplicationUI.getConn(),id);
		if(korisnik == null) {
			System.out.println("Ne postoji korisnik ciji je id " + id);
		}else {
			if(knjige.size() == 0) {
				System.out.println("Korisnik ciji je id " + id + " nije kupio nijednu knjigu");
			}else {
				System.out.println("Knjige korisnika " + korisnik.getIme() + " " +
						korisnik.getPrezime() + " su");
				biblioteka(knjige); 
			}
		}
	}
	public static void biblioteka(ArrayList<Knjiga> knjige) {
		Integer[] ids = new Integer[knjige.size()];
		String [] nazivi = new String [knjige.size()];
		Double[] cene = new Double[knjige.size()];
		for(int i = 0; i < knjige.size(); i++) {
			Knjiga knjiga = knjige.get(i);
			ids[i] = knjiga.getId();
			nazivi[i] = knjiga.getNaziv();
			cene[i] = knjiga.getCena();
		}
		ColumnFormatter<Number> idFormater = ColumnFormatter.number(Alignment.RIGHT,3, Precision.ZERO);
		ColumnFormatter<String> nazivFormater = ColumnFormatter.text(Alignment.LEFT,30);
		ColumnFormatter<Number> cenaFormater = ColumnFormatter.number(Alignment.LEFT,7, Precision.TWO);
		Table.Builder builder = new Table.Builder("Id", ids, idFormater);
		builder.addColumn("Naziv", nazivi, nazivFormater);
		builder.addColumn("Cena", cene, cenaFormater);

		Table table = builder.build();
		System.out.println(table);
	}
}
