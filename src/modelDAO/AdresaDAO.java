package modelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Adresa;

public class AdresaDAO {
	public static Adresa getAddressById(Connection conn, int id) {
		Adresa adresa = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT ad.ulica,ad.broj,ad.grad,ad.drzava FROM knjizara.adresa ad where adresa_id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				String ulica = rset.getString(index++);
				int broj = rset.getInt(index++);
				String grad = rset.getString(index++);
				String drzava = rset.getString(index++);
				if(grad == null) {
					adresa = new Adresa(ulica, broj, drzava);
				}else {
					adresa = new Adresa(ulica, broj, grad, drzava);
				}
				
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return adresa;
	}
	

}
