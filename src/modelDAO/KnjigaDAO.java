package modelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import model.Knjiga;

public class KnjigaDAO {
	public static ArrayList<Knjiga> getAll(Connection conn){
		ArrayList<Knjiga> knjige = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM knjizara.knjige;";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			//ovde sam pri jednom kontaktiranju baze uzela i sve podatke iz adrese da bih je napravila
			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				Knjiga knjiga = new Knjiga(id, naziv, cena);
			knjige.add(knjiga);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return knjige;
		}
	public static ArrayList<Knjiga> knjigeKupljeneUSrbiji(Connection conn){
		ArrayList<Knjiga> knjige = new ArrayList<>();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select knj.knjiga_id,knj.naziv,knj.cena from " + 
					"knjizara.knjige knj join " + 
					"knjizara.jekupio jk on knj.knjiga_id = knjiga " + 
					"join knjizara.korisnik kor on jk.korisnik = kor.korisnik_id " + 
					"join knjizara.adresa ad on kor.adresa = "
					+ "ad.adresa_id where ad.drzava = 'Srbija';";

			pstmt = conn.prepareStatement(query);
			int index = 1;

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				Knjiga knjiga = new Knjiga(id,naziv, cena);
				knjige.add(knjiga);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return knjige;
	}
	public static ArrayList<Knjiga> knjigeCijiJeKupac(Connection conn, int id){
		ArrayList<Knjiga> knjige= new ArrayList<>();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "Select pr.knjiga_id,pr.naziv, pr.cena "+
					"FROM knjizara.korisnik kor join knjizara.jekupio jk on"+
					" kor.korisnik_id = jk.korisnik join knjizara.knjige pr on"
					+ " jk.knjiga = pr.knjiga_id WHERE kor.korisnik_id = ? ;";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id); 

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int idK = rset.getInt(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				Knjiga knjiga = new Knjiga(idK,naziv, cena);
				knjige.add(knjiga);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return knjige;
	}
	public static ArrayList<Knjiga> knjigeKojeNikoNijeKupio(Connection conn){
		ArrayList<Knjiga> knjige = new ArrayList<>();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "select pr.knjiga_id,pr.naziv,pr.cena from " + 
					"knjizara.knjige pr left join " + 
					"knjizara.jekupio jk on pr.knjiga_id = knjiga " + 
					"where jk.kolicina is null;";

			pstmt = conn.prepareStatement(query);
			int index = 1;

			rset = pstmt.executeQuery();

			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				Knjiga knjiga = new Knjiga(id,naziv, cena);
				knjige.add(knjiga);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return knjige;
	}
	public static HashMap<String, Integer > sortiraniPoBrojuKupljenihKnjiga(Connection conn) {
		HashMap<String, Integer > rezultat = new LinkedHashMap<String, Integer>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "select ad.drzava ,sum(jk.kolicina) from " + 
					"knjizara.knjige pr left join " + 
					"knjizara.jekupio jk on pr.knjiga_id = knjiga " + 
					"join knjizara.korisnik kor on jk.korisnik = kor.korisnik_id " + 
					"join knjizara.adresa ad on kor.adresa = ad.adresa_id " + 
					"group by ad.drzava " + 
					"order by sum(jk.kolicina) desc;";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				String drzava = rset.getString(index++);
				int sum = rset.getInt(index++);
				rezultat.put(drzava, sum);
			}
		}
		catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return rezultat;
	}

}
