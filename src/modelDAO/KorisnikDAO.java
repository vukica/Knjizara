package modelDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import model.Adresa;
import model.Korisnik;

public class KorisnikDAO {
	public static List<Korisnik> getAllSaProsirenimAdresama(Connection conn) {
		List<Korisnik> korisnici = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT kor.ime,kor.prezime,kor.email,ad.ulica,ad.broj,ad.grad,ad.drzava " + 
					"FROM knjizara.korisnik kor " + 
					"join knjizara.adresa ad on kor.adresa = ad.adresa_id ;";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			//ovde sam pri jednom kontaktiranju baze uzela i sve podatke iz adrese da bih je napravila
			while (rset.next()) {
				int index = 1;
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				String email = rset.getString(index++);
				String ulica = rset.getString(index++);
				int broj = rset.getInt(index++);
				String grad = rset.getString(index++);
				if(grad == null) {
					String drzava = rset.getString(index++);
					Adresa adresa = new Adresa(ulica, broj,drzava);
					Korisnik korisnik = new Korisnik(ime,prezime,email,adresa);
					korisnici.add(korisnik);
				}else {
					String drzava = rset.getString(index++);
					Adresa adresa = new Adresa(ulica, broj, grad, drzava);
					Korisnik korisnik = new Korisnik(ime,prezime,email,adresa);
					korisnici.add(korisnik);
				}

			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return korisnici;
	}
	public static Korisnik getKorisnikById(Connection conn, int id) {
		Korisnik korisnik = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT kor.ime, kor.prezime, kor.email,kor.adresa FROM knjizara.korisnik kor WHERE kor.korisnik_id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id); 

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				String email = rset.getString(index++);
				int adresa_id = rset.getInt(index++);
				Adresa adresa = AdresaDAO.getAddressById(conn, adresa_id);
				korisnik = new Korisnik(ime, prezime, email, adresa);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return korisnik;
	}

	public static ArrayList<Korisnik> sviKupciIzSvajcarske(Connection conn) {
		ArrayList<Korisnik> korisnici = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "Select kor.ime,kor.prezime,kor.email,ad.adresa_id FROM "
					+ "knjizara.korisnik kor join knjizara.jekupio jk on " + 
					"kor.korisnik_id = jk.korisnik join knjizara.knjige pr on " + 
					"jk.knjiga = pr.knjiga_id join knjizara.adresa ad on kor.adresa = "
					+ "ad.adresa_id" + " WHERE ad.drzava = 'Svajcarska';";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				String email = rset.getString(index++);
				int adresa_id = rset.getInt(index++);
				Adresa adresa = AdresaDAO.getAddressById(conn, adresa_id);

				Korisnik korisnik = new Korisnik(ime,prezime,email,adresa);
				korisnici.add(korisnik);
			}
		}
		catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return korisnici;
	}
	public static HashMap<Korisnik, Integer > sortiraniPoBrojuKupljenihKnjiga(Connection conn) {
		HashMap<Korisnik, Integer > rezultat = new LinkedHashMap<Korisnik, Integer>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "select kor.korisnik_id, sum(jk.kolicina)from(knjizara.jekupio "
					+ "jk right join knjizara.korisnik kor on " + 
					"jk.korisnik = kor.korisnik_id) " + 
					" group by jk.korisnik " + 
					" order by sum(jk.kolicina) DeSC;";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int idKorisnika = rset.getInt(index++);
				int sum = rset.getInt(index++);
				Korisnik korisnik = getKorisnikById(conn, idKorisnika);
				rezultat.put(korisnik, sum);
			}
		}
		catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return rezultat;
	}
	public static ArrayList<Korisnik> sviKupciCijiJeGradNull(Connection conn) {
		ArrayList<Korisnik> korisnici = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "select kor.ime,kor.prezime,kor.email,ad.ulica,"
					+ "ad.broj,ad.drzava from knjizara.korisnik"
					+ " kor join knjizara.adresa ad on kor.adresa = ad.adresa_id"
					+ " where ad.grad is null;";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				String email = rset.getString(index++);
				String ulica = rset.getString(index++);
				int broj = rset.getInt(index++);
				//	String grad = rset.getString(index++); izbacila sam ga iz upita jer je svakako null
				//pa ga necu koristiti u konstruktoru
				String drzava = rset.getString(index++);
				Adresa adresa = new Adresa(ulica, broj,drzava);
				Korisnik korisnik = new Korisnik(ime,prezime,email,adresa);
				korisnici.add(korisnik);
			}
		}
		catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return korisnici;
	}
	public static HashMap<String, Integer > sortiraniDrzavaPoUkupnoKorisnika(Connection conn) {
		HashMap<String, Integer > rezultat = new LinkedHashMap<String, Integer>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "select ad.drzava, count(kor.korisnik_id)"
					+ "from(knjizara.korisnik kor right join " + 
					" knjizara.adresa ad on  " + 
					"kor.adresa = ad.adresa_id) " + 
					"group by ad.drzava " + 
					"order by count(kor.korisnik_id) DeSC; ";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				String drzava = rset.getString(index++);
				int sum = rset.getInt(index++);
				rezultat.put(drzava, sum);
			}
		}
		catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return rezultat;
	}
	public static HashMap<String, Double > sortiraniDrzavaPoUkupnojZaradi(Connection conn) {
		HashMap<String, Double > rezultat = new LinkedHashMap<String, Double>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "select ad.drzava, sum(jk.kolicina*pr.cena)"
					+ "from(knjizara.adresa ad left join " + 
					" knjizara.korisnik kor on " + 
					"ad.adresa_id = kor.adresa) " + 
					"left join knjizara.jekupio jk on jk.korisnik "
					+ "= kor.korisnik_id left join " + 
					" knjizara.knjige pr on jk.knjiga = pr.knjiga_id " + 
					" group by ad.drzava " + 
					"order by sum(jk.kolicina*pr.cena) DeSC;";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				String drzava = rset.getString(index++);
				double sum = rset.getDouble(index++);
				rezultat.put(drzava, sum);
			}
		}
		catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return rezultat;
	}
}
